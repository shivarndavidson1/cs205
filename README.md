# CS205

## My Game Idea

An RPG taking place on K'rd where you have to own the streets and take out the Rats.

I will have 3 types of rats:

 1. Standard Infantry
 1. Ranged
 1. Heavy

### Wishlist

This is stuff I want to bring in later.

 - Grenades
 - K'rd [Doornuts](https://www.thepiepiper.co.nz/) recipes

 ![Yes](./git-resources/yes.jpg)