﻿namespace _2_Adventure {
    public struct Weapon {
        public enum WeaponType {
            SWORD
        ,   KNIFE
        ,   BOW
        }

        public string name;
        public WeaponType weaponType;
        public int mp;
        public int strength;
        public int intelligence;
        public int accuracy;

        
    }
}
