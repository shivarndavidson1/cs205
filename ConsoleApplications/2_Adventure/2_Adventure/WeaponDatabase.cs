﻿using System.Collections.Generic;

namespace _2_Adventure {
    class WeaponDatabase {
        public static Dictionary<string, Weapon> Swords = new Dictionary<string, Weapon> { };
        public static Dictionary<string, Weapon> Knives = new Dictionary<string, Weapon> { };
        public static Dictionary<string, Weapon> Bows = new Dictionary<string, Weapon> { };

    }
}
