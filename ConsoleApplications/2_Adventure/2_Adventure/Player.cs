﻿using System.Collections.Generic;

namespace _2_Adventure {
    class Player {
        public Weapon.WeaponType weaponType;
        public Weapon weapon;
        public Armor armor;
        public int expToNextLevel = 200;

        private int level = 1;
        private Growth growth;
        private readonly Dictionary<string, Ability> abilities;

        private Weapon SetDefaultWeapon() {
            return new Weapon();
        }

        private Armor SetDefaultArmor() {
            return new Armor();
        }

        public Ability PresentOptions() {
            return new Ability();
        }

        public void LevelUp() {

        }
    }
}
