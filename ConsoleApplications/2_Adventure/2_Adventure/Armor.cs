﻿using System.Security.Permissions;

namespace _2_Adventure {
    public struct Armor {
        public string name;
        public int hp;
        public int defense;
        public int spirit;
        public int evasion;
    }
}
