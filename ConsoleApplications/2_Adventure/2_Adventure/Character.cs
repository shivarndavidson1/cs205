﻿namespace _2_Adventure {
    public abstract class Character {
        public string name;
        public int hp;
        public int maxHP;
        public int mp;
        public int maxMP;
        public int strength;
        public int defense;
        public int intelligence;
        public int spirit;
        public int accuracy;
        public int evasion;
        public int luck;
        public int exp;
        public int qtyPotions;

        protected void SetStats() {

        }

        public int GetCriticalMultiplier() {
            return 1;
        }
    }
}
