﻿using System;
using System.Xml;

namespace _2_Adventure {
    public struct Ability {
        public string name;
        public int mpCost;
        public float power;
        public Action<Character, float> action;
    }
}
