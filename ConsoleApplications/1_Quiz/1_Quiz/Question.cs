﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;

namespace _1_Quiz {
    class Question {
        public Question(string _question, Dictionary<string, Action> _responses) {
            question = _question;
            responses = _responses;

            Ask();
        }

        public string question = "";
        public Dictionary<string, Action> responses;

        public void Ask()
        {
            string input = "";
            int index = 0;

            while (!responses.ContainsKey(input))
            {
                ShowQuestion();
                ShowOptions();

                ProvideFeedbackInvalid(input);

                input = Console.ReadLine();

                try
                {
                    index = int.Parse(input);
                    input = responses.ElementAt(index - 1).Key;
                }
                catch (Exception)
                {

                    index = -1;
                }
            }
            responses[input]();
        }
        private void ProvideFeedbackInvalid(string _input) {
            if (!string.IsNullOrEmpty(_input)) {
                Console.WriteLine("Invalid input. Try Again.");
            }
        }

        private void ShowQuestion() {
            Console.WriteLine(question);
        }

        private void ShowOptions() {
            string options = "";
            int index = 0;

            foreach (KeyValuePair<string, Action> kvp in responses) { 
                options += ++ index + ": " + kvp.Key + "\n";

            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(options);
            Console.ResetColor();
        }
    }
}
